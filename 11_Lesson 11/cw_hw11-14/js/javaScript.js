let count = 0, count2 = 0, count3 = 0, amount1 = 0, amount2 = 0, amount3 = 3;
        
const priceTable = {
     firstPrice : '100',
     secondPrice : '200',
     threePrice: '300'
 }

window.addEventListener("DOMContentLoaded", function(e){
     const small = document.querySelector("#small"),
     mid = document.querySelector("#mid"),
     big = document.querySelector("#big"),
     span = document.createElement("span"),
     div = document.querySelector(".price > p");
     //Радио кнопки
     small.addEventListener('click', function(){
         span.textContent = `${priceTable.firstPrice}`;
         div.append(span);
     })

    mid.addEventListener('click', function(){
         span.textContent = `${priceTable.secondPrice}`;
         div.append(span)
     })

    big.addEventListener('click', function(){
         span.textContent = `${priceTable.threePrice}`;
         div.append(span)
     })

     //Выбор соусов
     const catsup =  document.querySelector("#sauceClassic"),
     bbq= document.querySelector('#sauceBBQ'),
     rikots = document.querySelector('#sauceRikotta'),
     imgForSauces = document.createElement('img'),
     nameOfSaucec = document.querySelector('.sauces > p'),
     spanOfSauces = document.createElement('span'),
     saucesForTable = document.querySelector(".table"),
     sumSpanSauces = document.createElement('span');

     const theNameOfTheSauces = {
         'ketchups': ' Кетчуп ',
         'bbq': ' BBQ ',
         'rikota': ' Рікота '
     }

     const sauces = {
         'ketchup': catsup.src,
         'barbecue': bbq.src,
         'ricotta': rikots.src
     }

     const saucesPrice = {
         'ketchupPrice': '15',
         'barbecuePrice': '20',
         'ricottaPrice': '25'
     }

     catsup.addEventListener('click', ()=>{
         imgForSauces.src = sauces.ketchup;
         saucesForTable.append(imgForSauces);
         
         spanOfSauces.textContent = theNameOfTheSauces.ketchups;
         nameOfSaucec.append(spanOfSauces);

         sumSpanSauces.textContent = saucesPrice.ketchupPrice;
     })

     bbq.addEventListener('click', ()=>{
         imgForSauces.src = sauces.barbecue;
         saucesForTable.append(imgForSauces);

         spanOfSauces.textContent = theNameOfTheSauces.bbq;
         nameOfSaucec.append(spanOfSauces);

         sumSpanSauces.textContent = saucesPrice.barbecuePrice;
     })

     rikots.addEventListener('click',()=>{
         imgForSauces.src = sauces.ricotta;
         saucesForTable.append(imgForSauces);
         
         spanOfSauces.textContent = theNameOfTheSauces.rikota;
         nameOfSaucec.append(spanOfSauces);

         sumSpanSauces.textContent = saucesPrice.ricottaPrice;
     })
     

     //Топинги(Сыры)
     const topingsForCheese = document.querySelector('.table'),
     cheese = document.querySelector('#moc1'),
     feta = document.querySelector('#moc2'),
     mozza = document.querySelector('#moc3'),
     imgForCheese = document.createElement('img'),
     spanSumToping = document.createElement('span'),
     nameOfToping = document.createElement('span'),
     spanOfToping = document.querySelector('.topings > p');

     const theNameOfTheTopings = {
         'cheeseName': ' Сир звичайний ',
         'sirFetaName': ' Сир фета ',
         'mozzarellaName': ' Моцарелла '
     }

     const toping = {
         'cheese': cheese.src, 
         'sirFeta': feta.src,
         'mozzarella': mozza.src
     }

     const topingPrice ={
         'cheesePrice': '20',
         'sirFetaPrice': '30',
         'mozzarellaPrice': '35'
     }

     cheese.addEventListener('click', ()=>{
         imgForCheese.src = toping.cheese;
         topingsForCheese.append(imgForCheese)

         nameOfToping.textContent = theNameOfTheTopings.cheeseName;
         spanOfToping.append(nameOfToping);

         spanSumToping.textContent = topingPrice.cheesePrice;
     })

     feta.addEventListener('click', ()=>{
         imgForCheese.src = toping.sirFeta;
         topingsForCheese.append(imgForCheese);

         nameOfToping.textContent = theNameOfTheTopings.sirFetaName;
         spanOfToping.append(nameOfToping);

         spanSumToping.textContent = topingPrice.sirFetaPrice;
     })

     mozza.addEventListener('click',()=>{
         imgForCheese.src = toping.mozzarella;
         topingsForCheese.append(imgForCheese);

         nameOfToping.textContent = theNameOfTheTopings.mozzarellaName;
         spanOfToping.append(nameOfToping);
         
         spanSumToping.textContent = topingPrice.mozzarellaPrice;
     })

    

     //Топинги(Мясо)
     const topingForMeetAndVege = document.querySelector(".table"),
     telya = document.querySelector("#telya"),
     tomat = document.querySelector("#vetch1"),
     mushroom = document.querySelector("#vetch2"),
     imgForMeet = document.createElement("img"),
     nameOfMeetVeg = document.createElement("span"),
     spanOfMeetVeg = document.querySelector(".topings > p"),
     sumSpanMeetVeg = document.createElement('span');

     const theNameOfTheMeetAndVeg = {
         'vealName': 'Телятина',
         'tomatoesName': 'Помідори',
         'mushroomsName': 'Гриби'
     }

     const topingMeetVeg = {
         'veal': telya.src, 
         'tomatoes': tomat.src,
         'mushrooms': mushroom.src
     }

     const topingMeetVegPrice = {
         'vealPrice': '30', 
         'tomatoesPrice': '20',
         'mushroomsPrice': '25'
     }

     telya.addEventListener('click', ()=>{
         imgForMeet.src = topingMeetVeg.veal;
         topingForMeetAndVege.append(imgForMeet)

         nameOfMeetVeg.textContent = theNameOfTheMeetAndVeg.vealName;
         spanOfMeetVeg.append(nameOfMeetVeg);

         sumSpanMeetVeg.textContent = topingMeetVegPrice.vealPrice;
     })

     tomat.addEventListener('click', ()=>{
         imgForMeet.src = topingMeetVeg.tomatoes;
         topingForMeetAndVege.append(imgForMeet);

         nameOfMeetVeg.textContent = theNameOfTheMeetAndVeg.tomatoesName;
         spanOfMeetVeg.append(nameOfMeetVeg);

         sumSpanMeetVeg.textContent = topingMeetVegPrice.tomatoesPrice;
     })

     mushroom.addEventListener('click',()=>{
         imgForMeet.src = topingMeetVeg.mushrooms;
         topingForMeetAndVege.append(imgForMeet);

         nameOfMeetVeg.textContent = theNameOfTheMeetAndVeg.mushroomsName;
         spanOfMeetVeg.append(nameOfMeetVeg);

         sumSpanMeetVeg.textContent = topingMeetVegPrice.mushroomsPrice;
     })

     //Бегающая кнопка скидки
     function getRandomBanner(min,max){
         return Math.floor(Math.random() * (max - min + 1))
     }
     const submit = document.querySelector("#banner > button");

     submit.addEventListener("click", function(e){
         const banner = document.getElementById("banner");
         banner.style.right = getRandomBanner(0, 450) + "px";
         banner.style.bottom = getRandomBanner(0, 450) + "px";
     })

     //Валидация
     //Проверка имени
     function personalitiName(type = ''){
       const name = document.getElementById("name");
       name.type = type;
       return name;
     }
     const userName = {
         personalName : personalitiName('text')
     }
     userName.personalName.addEventListener("change", ()=>{
         let user = /[А-яіІїЇєЄ]{2,}/;
         if(user.test(userName.personalName.value)){
             userName.personalName.classList.add("success")
             userName.personalName.classList.remove("error")
             count++;
             console.log(true);
         }else{
             userName.personalName.classList.add("error")
             userName.personalName.classList.remove("success")
             console.log(false)
         }
     })
     //Проверка телефона
     function personalitiTel(type = ''){
         const tel = document.getElementById("number");
         tel.type = type;
         return tel;
     }
     const userTel = {
         personalTel : personalitiTel('text')
     }
     userTel.personalTel.addEventListener("change", ()=>{
         let telephone = /\+38\(\d{3}\)\d{3}-\d{2}-\d{2}/
         if(telephone.test(userTel.personalTel.value)){
             userTel.personalTel.classList.add("success")
             userTel.personalTel.classList.remove("error")
             count2++;
             console.log(true);
         }else{
             userTel.personalTel.classList.add("error")
             userTel.personalTel.classList.remove("success")
             console.log(false)
         }
     })
     //Проверка почты
     function personalitiEmail(type = ''){
         const email = document.getElementById("email");
         email.type = type;
         return email;
     }
     const userEmail = {
         personalEmail : personalitiEmail('email')
     }
     userEmail.personalEmail.addEventListener("change", ()=>{
         let mail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/
         if(mail.test(userEmail.personalEmail.value)){
             userEmail.personalEmail.classList.add("success")
             userEmail.personalEmail.classList.remove("error")
             count3++;
             console.log(true);
         }else{
             userEmail.personalEmail.classList.add("error")
             userEmail.personalEmail.classList.remove("success")
             console.log(false)
         }
     })
     
     const submitForInfo = document.querySelector(".button + .button");

     const span3 = document.createElement("span");
     submitForInfo.addEventListener("click", ()=>{
         if(count !== 0 && count2 !== 0 && count3 !== 0){
             document.location.href = "./thank-you.html";
             span3.textContent = (+span.textContent) + (+sumSpanSauces.textContent) + (+spanSumToping.textContent) + (+sumSpanMeetVeg.textContent);
             localStorage.setItem('name',  JSON.stringify(userName.personalName.value));
             localStorage.setItem('tel', JSON.stringify(userTel.personalTel.value));
             localStorage.setItem('email', JSON.stringify(userEmail.personalEmail.value));
             localStorage.setItem('value', JSON.stringify(span3.textContent));
             count = 0,count2 = 0,count3 = 0;
             console.log(true);
         }else{
             console.log(false);
             userName.personalName.value = '';
             userTel.personalTel.value = '';
             userEmail.personalEmail.value = '';
             count = 0,count2 = 0,count3 = 0;
         }
     })
 })